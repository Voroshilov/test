<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">

            <!-- Branding Image -->
            <a class="navbar-brand" onclick="javascript:history.back();">back</a>
            <a class="navbar-brand" href="{{ url('/') }}">Welcome</a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                <li><a href="{{ url('/users') }}">Users</a></li>
            </ul>
            <!-- Right Side Of Navbar -->
        </div>
    </div>
</nav>