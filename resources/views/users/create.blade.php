@extends('layouts.app')

@section('title')
    <title>Create user</title>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ action('UserController@store') }}">
                            <div class="form-group">
                                {!! Form::label('name', 'Name:', ['class' => 'control-label']) !!}
                                {!! Form::text('name', null , ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('email', 'Email:', ['class' => 'control-label']) !!}
                                {!! Form::text('email', null , ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('address', 'Address:', ['class' => 'control-label']) !!}
                                {!! Form::text('address', null , ['class' => 'form-control']) !!}
                            </div>
                            <div align="right">
                                {!! Form::submit('Update user', ['class' => 'btn btn-primary']) !!}
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
