@extends('layouts.app')

@section('title')
    <title>Users</title>
@endsection

@section('content')

    <script>
        function confirmation()
        {
            if(confirm('Confirm')){
                document.getElementById('yn').value = 1;
            }else{
                document.getElementById('yn').value = 0;
            }
            document.forms['form1'].submit();
        }
    </script>

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="{{action('UserController@create')}}">create user</a>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped">
                            <tr>
                                <td><div><a tabindex="-1" href="#">ID</a></div></td>
                                <td><div><a tabindex="-1" href="#">Name</a></div></td>
                                <td><div><a tabindex="-1" href="#">E-mail</a></div></td>
                                <td><div><a tabindex="-1" href="#">Date create</a></div></td>
                                <td>
                                </td>
                            </tr>

                            @foreach ($users as $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>
                                        <div>{{ $user->name }}</div>
                                    </td>
                                    <td>
                                        <div>{{ $user->email }}</div>
                                    </td>
                                    <td>
                                        <div>{{ $user->created_at }}</div>
                                    </td>
                                    <td class="actions">
                                        <div>
                                            <a class="btn" href="{{action('UserController@edit', $user->id)}}"><i class="fa fa-pencil"></i></a>
                                        </div>

                                        {!! Form::open( [ 'method' => 'DELETE', 'name' =>'form1', 'action' => ['UserController@destroy', $user->id] ]) !!}

                                            <input type = hidden name="confirm" id="yn" value = 0>
                                            <button class="btn" type="submit" onClick="confirmation()"><i class="fa fa-trash"></i></button>

                                        {!! Form::close() !!}

                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
