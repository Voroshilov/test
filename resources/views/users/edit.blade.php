@extends('layouts.app')

@section('title')
    <title>User {{$user->name}}</title>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">

                        {!! Form::model($user, ['method' => 'PATCH','action' => ['UserController@update', $user->id]]) !!}
                        <div class="form-group">
                            {!! Form::label('name', 'Name:', ['class' => 'control-label']) !!}
                            {!! Form::text('name', $user->name , ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('email', 'Email:', ['class' => 'control-label']) !!}
                            {!! Form::text('email', $user->email , ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('address', 'Address:', ['class' => 'control-label']) !!}
                            {!! Form::text('address', $user->address , ['class' => 'form-control']) !!}
                        </div>
                        <div align="right">
                            {!! Form::submit('Update user', ['class' => 'btn btn-primary']) !!}
                        </div>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://api-maps.yandex.ru/1.1/index.xml" type="text/javascript"></script>
    <script>
        YMaps.jQuery(function () {
            var map = new YMaps.Map(YMaps.jQuery("#YMapsID")[0]);
            map.addControl(new YMaps.TypeControl());
            map.addControl(new YMaps.ToolBar());
            map.addControl(new YMaps.Zoom());
            map.addControl(new YMaps.MiniMap());
            map.addControl(new YMaps.ScaleLine());


            var long = '{{$user->longitude}}';
            var lat = '{{$user->latitude}}';
            var center = new YMaps.GeoPoint(long, lat);
            var placemark =  new YMaps.Placemark(new YMaps.GeoPoint(long,lat));

            placemark.name = '{{$user->name}}';
            placemark.description = '{{$user->address}}';

            map.addOverlay(placemark);

            map.setCenter((center), 13);
        })
    </script>

    <div class="col-md-12"  id="YMapsID" style="height:550px"></div>

@endsection
