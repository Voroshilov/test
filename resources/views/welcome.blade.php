@extends('layouts.app')

@section('content')
    <script src="https://api-maps.yandex.ru/1.1/index.xml" type="text/javascript"></script>
    <script>
        // Создает обработчик события window.onLoad
        YMaps.jQuery(function () {

            var map = new YMaps.Map(YMaps.jQuery("#YMapsID")[0]);
            map.addControl(new YMaps.TypeControl());
            map.addControl(new YMaps.ToolBar());
            map.addControl(new YMaps.Zoom());
            map.addControl(new YMaps.MiniMap());
            map.addControl(new YMaps.ScaleLine());

            {{--var long = '{{$user->longitude}}';--}}
            {{--var lat = '{{$user->latitude}}';--}}
            var center = new YMaps.GeoPoint(36.233448, 49.991484);

            var arr = ["Яблоко", "Апельсин", "Груша"];

            arr.forEach(function(item, i, arr) {
                alert( i + ": " + item + " (массив:" + arr + ")" );
            });

            var placemark = [];

            placemark[0] = new YMaps.Placemark(new YMaps.GeoPoint(long,lat));
            placemark[1] = new YMaps.Placemark(new YMaps.GeoPoint(36.233448,49.991484));

            placemark.name = '{{$user->name}}';
            placemark.description = '{{$user->address}}';

            map.addOverlay(placemark);

            map.setCenter((center), 13);
        })
    </script>

    <div class="col-md-12"  id="YMapsID" style="height:700px"></div>

@endsection