@extends('layouts.app')

@section('content')
    <script src="https://api-maps.yandex.ru/1.1/index.xml" type="text/javascript"></script>
    <script>
        // Создает обработчик события window.onLoad
        YMaps.jQuery(function () {

            var map = new YMaps.Map(YMaps.jQuery("#YMapsID")[0]);
            map.addControl(new YMaps.TypeControl());
            map.addControl(new YMaps.ToolBar());
            map.addControl(new YMaps.Zoom());
            map.addControl(new YMaps.MiniMap());
            map.addControl(new YMaps.ScaleLine());

            var center = new YMaps.GeoPoint(36.233448, 49.991484);

            var placemark = [];

            <?php $i = 0; ?>
            @foreach($users as $user)
                placemark['{{$i}}'] = new YMaps.Placemark(new YMaps.GeoPoint('{{$user->longitude}}','{{$user->latitude}}'));

                placemark['{{$i}}'].name = '{{$user->name}}';
                placemark['{{$i}}'].description = '{{$user->address}}';
            <?php $i++;?>
            @endforeach

            map.addOverlay(placemark);

            map.setCenter((center), 13);
        })
    </script>

    <div class="col-md-12"  id="YMapsID" style="height:700px"></div>

@endsection