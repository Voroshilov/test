@extends('layouts.app')

@section('title')
    <title>{{Auth::user()->name}}</title>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Home page</div>

                <div class="panel-body">
                    You are home!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
