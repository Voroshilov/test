<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getAddress($data)
    {
        $params = array(
            'geocode' => $data['address'],
            'format'  => 'json',
            'results' => 1,
            'key'     => 'AML1CVMBAAAAbQy7PwIA97S9R_k8lCiLwoDS37kFxQWjVR4AAAAAAAAAAACPBQZS73AHrmVL3hvBz1zsELf0Uw==',
        );
        $response = json_decode(file_get_contents('http://geocode-maps.yandex.ru/1.x/?'.http_build_query($params, '', '&')));
        if ($response->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0)
        {
            $text = $response->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos;
            $newText = explode(' ' , $text);

            $data['latitude'] = $newText[1];
            $data['longitude'] = $newText[0];
        }
        else
        {
            echo 'Ничего не найдено';
        }

        return $data;
    }
}
